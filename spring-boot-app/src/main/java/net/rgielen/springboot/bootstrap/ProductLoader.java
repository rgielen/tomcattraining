package net.rgielen.springboot.bootstrap;

import net.rgielen.springboot.domain.Product;
import net.rgielen.springboot.repositories.ProductRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ProductLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final ProductRepository productRepository;

    private Logger log = Logger.getLogger(ProductLoader.class);

    @Autowired
    public ProductLoader(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        long productCount = productRepository.count();

        if(productCount == 0){
            Product foo = new Product();
            foo.setDescription("Foo Gadget");
            foo.setPrice(new BigDecimal("19.99"));
            foo.setProductId("abc-1234");
            productRepository.save(foo);

            Product bar = new Product();
            bar.setDescription("Bar Thingy");
            bar.setPrice(new BigDecimal("2.29"));
            bar.setProductId("cde-5432");
            productRepository.save(bar);
        }

    }
}
