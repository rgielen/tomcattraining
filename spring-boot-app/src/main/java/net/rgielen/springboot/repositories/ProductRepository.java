package net.rgielen.springboot.repositories;

import net.rgielen.springboot.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer>{
}
