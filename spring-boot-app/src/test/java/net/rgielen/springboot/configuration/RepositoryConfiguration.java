package net.rgielen.springboot.configuration;

import net.rgielen.springboot.domain.Product;
import net.rgielen.springboot.repositories.ProductRepository;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackageClasses = {Product.class})
@EnableJpaRepositories(basePackageClasses = {ProductRepository.class})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
